package calendari;

public class NextDayTest {
	public static void main(String[] args) {
		init();

	}

	public static void init() {
		NextDay nextDay = new NextDay();

		String resultat = nextDay.calcula(30, 2, 1904); // 10 de febrer del 2001

		System.out.println(resultat); // Mostra 11/2/2001
	}

}