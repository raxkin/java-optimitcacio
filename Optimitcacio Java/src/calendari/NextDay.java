package calendari;

public class NextDay {

	private int[] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	//Calulo quin es el seguent dia de la data rebuda
	public String calcula(int dia, int mes, int any) {
		checkDate(dia, mes, any);
		if(dia == getDaysMonth(mes, any)) {
			dia = 1;
			mes++;
			if(mes > 12) {
				any++;
				mes = 1;
			}
		}
		else
			dia++;

		return dia + "/" + mes + "/" + any;
	}

	//Comprobo cuants dies te el mes
	private int getDaysMonth(int mes, int any) {
		if (mes == 2) {
			if (isBisiest(any)) {
				return days[mes - 1] + 1;
			}
		}
		return days[mes - 1];
	}

	//Comprobo si el any es bisiest
	private boolean isBisiest(int any) {
		if ((any % 4 == 0) && (any % 100 != 0 || any % 400 == 0))
			return true;
		else
			return false;
	}
	
	//
	private void checkDate(int dia, int mes, int any) {
		if(dia > getDaysMonth(mes, any) || dia < 1 )
			throw new IllegalArgumentException("Dia incorrecte: " + dia);
		
		if(mes > 12 || mes < 1)
			throw new IllegalArgumentException("Mes incorrecte: " + mes);
		
		if(any < 0)
			throw new IllegalArgumentException("Any incorrecte: " + any);
	}
}