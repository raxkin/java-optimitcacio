package words;

public class WordAnalyzerTest {

	public static void main(String[] args) {
		testFirstRepeatedCharacter("aardvark"); // expect: a
		testFirstRepeatedCharacter("roommate"); // expect: o (not m)
		testFirstRepeatedCharacter("mate"); // expect: 0 (no duplicate letters)
		testFirstRepeatedCharacter("test"); // expect: 0 (the t isn't repeating)

		testFirstMultipleCharacter("missisippi"); // expect: i (not m or s)
		testFirstMultipleCharacter("mate"); // expect: 0 (no duplicate letters)
		testFirstMultipleCharacter("test"); // expect: t

		testCountRepeatedCharacters("mississippiii"); // expect: 4 (ss, ss, pp, iii)
		testCountRepeatedCharacters("test"); // expect: 0 (no repeated letters)
		testCountRepeatedCharacters("aabbcdaaaabb"); // expect: 4 (aa, bb, aaaa, bb)
	}

	private static void testFirstRepeatedCharacter(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		char result = wa.firstRepeatedCharacter();
		if (result == 0)
			System.out.println("No repeated character.");
		else
			System.out.println("First repeated character = " + result);
	}

	private static void testFirstMultipleCharacter(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		char result = wa.firstMultipleCharacter();
		if (result == 0)
			System.out.println("No multiple character.");
		else
			System.out.println("First multiple character = " + result);
	}

	private static void testCountRepeatedCharacters(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		int result = wa.countRepeatedCharacters();
		System.out.println(result + " repeated characters.");
	}
}