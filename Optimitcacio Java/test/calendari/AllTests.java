package calendari;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnysBixestsTest.class, AnysNormalsTest.class })
public class AllTests {

}
