package calendari;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AnysBixestsTest {

	NextDay n;
	
	@Before
	public void generarDia() {
		n = new NextDay();
	}
	
	@Test
	public void testNoBixest() {
		String resultat = n.calcula(28, 2, 1994);
		assertEquals("1/3/1994", resultat);
	}
	
	@Test
	public void testBixest() {
		String resultat = n.calcula(28, 2, 1904);
		assertEquals("29/2/1904", resultat);
	}


}
