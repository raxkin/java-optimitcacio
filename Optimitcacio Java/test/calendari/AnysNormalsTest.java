package calendari;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnysNormalsTest {

	NextDay n;
	
	@Before
	public void generarDia() {
		n = new NextDay();
	}
	
	@Test
	public void testDiaNormal() {
		String resultat = n.calcula(24, 3, 1994);
		assertEquals("25/3/1994", resultat);
	}
	
	@Test
	public void testCanviMes31() {
		String resultat = n.calcula(31, 3, 1994);
		assertEquals("1/4/1994", resultat);
	}
	
	@Test
	public void testCanviMes30() {
		String resultat = n.calcula(30, 4, 1994);
		assertEquals("1/5/1994", resultat);
	}
	
	@Test
	public void testCanviAny() {
		String resultat = n.calcula(31, 12, 1994);
		assertEquals("1/1/1995", resultat);
	}

}
